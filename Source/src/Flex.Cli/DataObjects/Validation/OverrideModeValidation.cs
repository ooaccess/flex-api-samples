using Flex.Cli.Hardware.Settings;
using FluentValidation;

namespace Flex.DataObjects.Validation
{
    internal class OverrideModeValidation : AbstractValidator<OverrideDoorModeSettings>
    {
        public OverrideModeValidation()
        {
            RuleFor(p => p.UniqueKey).NotEmpty();
            RuleFor(p => p.DoorMode).IsInEnum();
            RuleFor(p => p.Minutes).GreaterThanOrEqualTo(1).LessThanOrEqualTo(16383);
            RuleFor(p => p.Seconds).GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);

            RuleFor(p => p.OverrideMode).NotNull();
        }
    }
}
