
namespace Flex.DataObjects.Hardware
{
    public partial class DirectCommand
    {
        /// <summary>
        /// Unique key of the direct command
        /// </summary>
        public Guid UniqueKey { get; set; }

        /// <summary>
        /// Name of the direct command
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Category to group the direct command in.
        /// </summary>
        public string Category { get; set; }
    }
}
