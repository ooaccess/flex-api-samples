using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace Flex.DataObjects.Hardware
{
    internal class OverrideModeOptions
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public DoorMode DoorMode { get; set; }

        public int Duration { get; set; }
    }
}
