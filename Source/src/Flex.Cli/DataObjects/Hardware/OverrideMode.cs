
namespace Flex.DataObjects.Hardware
{
    internal enum OverrideMode
    {
        /// <summary>
        /// Sets the door mode on the selected door for the indicated amount of time. The number of minutes can be up to 16383 (over 11 days). The door will return to normal after the time has expired.
        /// </summary>
        Minutes,
        /// <summary>
        /// Allows the operator to change the door mode until a specified ending time (in hours/ minutes). 
        /// When using the Time of Day option, the mode will not necessarily end at the exact hour/minute you specify.
        /// Instead, it will last for a fixed number of whole minutes that is closest to the time specified.
        /// For example, if a door override mode was scheduled to end at 1:00:00 p.m., and it was 12:30:30 p.m.
        /// when the override was executed, the mode would end at 1:00:30 p.m.
        /// </summary>
        TimeOfDay,
        /// <summary>
        /// Overrides the door’s normal mode and sets the reader to the specified mode permanently. The override must be cancelled for the door to resume its normal state.
        /// </summary>
        Indefinite,
        /// <summary>
        /// Sets the door mode on the selected door for the indicated amount of time. The number of seconds can be up to 100 seconds. The door will return to normal after the time has expired.
        /// </summary>
        Seconds
    }
}
