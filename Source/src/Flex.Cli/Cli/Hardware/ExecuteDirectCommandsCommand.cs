using System.Net;
using Flex.Cli.Hardware.Settings;
using Flex.DataObjects;
using Flex.Services.Abstractions;
using Humanizer;
using Spectre.Console;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware
{
    internal class ExecuteDirectCommandsCommand : AsyncCommand<ExcuteDirectCommandSettings>
    {
        public ExecuteDirectCommandsCommand(IOptionsProvider options, ICacheStore cache, IFlexHttpClientFactory factory)
            : base(options, cache, factory)
        {
        }

        #region Overrides of AsyncCommand<ExcuteDirectCommandSettings>

        protected override async Task<int> ExecuteAsync(CommandContext context, ExcuteDirectCommandSettings settings, HttpClient client, UserInfo userInfo)
        {
            var response = await AnsiConsole
                .Status()
                .StartAsync("Sending door mode command...", _ => client.PostJSendAsync($"api/v2/hardware/directcommand/{settings.UniqueKey}/execute", null));

            if (!response.IsSuccess())
                return DisplayError(response);

            if (settings.OutputJson)
                DisplayJson(response.Data);
            else
                AnsiConsole.MarkupLine($"[green]Executed direct command[/] [yellow]{settings.UniqueKey}[/]");

            return CommandLineSuccess;
        }

        #endregion
    }
}
