using System.ComponentModel;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware.Settings
{
    internal class ExcuteDirectCommandSettings : HardwareSettings
    {
        /// <summary>
        /// Unique identifier for the direct command
        /// </summary>
        [CommandArgument(0, "<UNIQUE_KEY>")]
        [Description("Unique Identifier (GUID) of the direct command")]
        public virtual Guid? UniqueKey { get; set; }
    }
}
