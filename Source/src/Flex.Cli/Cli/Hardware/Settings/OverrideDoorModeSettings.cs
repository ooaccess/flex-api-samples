using System.ComponentModel;
using Flex.DataObjects.Hardware;
using FluentValidation;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware.Settings
{
    internal class OverrideDoorModeSettings : HardwareSettings
    {
        private readonly IValidator<OverrideDoorModeSettings> _validation;

        public OverrideDoorModeSettings(IValidator<OverrideDoorModeSettings> validation) => _validation = validation;

        #region Overrides of DefaultCommandSettings

        protected override FluentValidation.Results.ValidationResult InternalValidate() => _validation.Validate(this);

        #endregion

        /// <summary>
        /// Unique identifier for the hardware
        /// </summary>
        [CommandArgument(0, "<UNIQUE_KEY>")]
        [Description("Unique Identifier (GUID) of the hardware")]
        public virtual Guid? UniqueKey { get; set; }

        [CommandArgument(1, "<DOOR_MODE>")]
        public DoorMode DoorMode { get; set; }

        [CommandOption("-i|--indefinite")]
        public bool? Indefinite { get; set; }

        [CommandOption("-t|--time")]
        public DateTime? TimeOfDay { get; set; }
        
        [CommandOption("-m|--minutes")]
        public int? Minutes { get; set; }

        [CommandOption("-s|--seconds")]
        public int? Seconds { get; set; }

        internal OverrideMode? OverrideMode
        {
            get
            {
                if (Indefinite.HasValue && Indefinite.Value)
                    return DataObjects.Hardware.OverrideMode.Indefinite;
                if (TimeOfDay.HasValue)
                    return DataObjects.Hardware.OverrideMode.TimeOfDay;
                if (Minutes.HasValue)
                    return DataObjects.Hardware.OverrideMode.Minutes;
                if (Seconds.HasValue)
                    return DataObjects.Hardware.OverrideMode.Seconds;
                return null;
            }
        }
    }
}
