using System.Net;
using Flex.Cli.Hardware.Settings;
using Flex.DataObjects;
using Flex.DataObjects.Hardware;
using Flex.Services.Abstractions;
using Spectre.Console;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware
{
    internal class CancelOverrideModeCommand : AsyncCommand<CancelOverrideModeSettings>
    {
        public CancelOverrideModeCommand(IOptionsProvider options, ICacheStore cache, IFlexHttpClientFactory clientFactory)
            : base(options, cache, clientFactory)
        {
        }

        #region Overrides of AsyncCommand<CancelOverrideModeSettings>

        protected override async Task<int> ExecuteAsync(CommandContext context, CancelOverrideModeSettings settings, HttpClient client, UserInfo userInfo)
        {
            var right = userInfo[UserRights.AllowCtrlAcmMode];
            if (!right.AsBool())
            {
                AnsiConsole.MarkupLine("[red]{0}[/]", "Operator is not allowed to change the door mode");
                return CommandLineInsufficientPermission;
            }

            var response = await AnsiConsole.Status().StartAsync("Sending cancel override door mode command...", _ =>
                client.PostJSendAsync($"api/v2/hardware/door/{settings.UniqueKey}/overridemode",
                    new OverrideModeOptions
                    {
                        DoorMode = DoorMode.Disable,
                        Duration = 0
                    }));

            if (!response.IsSuccess())
                return DisplayError(response);

            if (settings.OutputJson)
                DisplayJson(response.Data);
            else
                AnsiConsole.MarkupLine("[green]Override door mode cancelled. Door should resume its normal state.[/]");

            return CommandLineSuccess;
        }

        #endregion
    }
}
