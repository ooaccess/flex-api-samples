using System;
using System.Net;
using Flex.Cli.Hardware.Settings;
using Flex.DataObjects;
using Flex.DataObjects.Hardware;
using Flex.Services.Abstractions;
using Humanizer;
using Spectre.Console;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware
{
    internal class OverrideDoorModeCommand : AsyncCommand<OverrideDoorModeSettings>
    {
        public OverrideDoorModeCommand(IOptionsProvider options, ICacheStore cache, IFlexHttpClientFactory factory)
            : base(options, cache, factory)
        {
        }

        #region Overrides of AsyncCommand<DoorModeSettings>

        protected override async Task<int> ExecuteAsync(CommandContext context, OverrideDoorModeSettings settings, HttpClient client, UserInfo userInfo)
        {
            var right = userInfo[UserRights.AllowCtrlAcmMode];
            if (!right.AsBool())
            {
                AnsiConsole.MarkupLine("[red]{0}[/]", "Operator is not allowed to change the door mode");
                return CommandLineInsufficientPermission;
            }

            var response = await AnsiConsole.Status().StartAsync("Sending door override mode command...", _ =>
                client.PostJSendAsync($"api/v2/hardware/door/{settings.UniqueKey}/overridemode",
                    new OverrideModeOptions
                    {
                        DoorMode = settings.DoorMode,
                        Duration = GetTimeInterval(settings)
                    }));

            if (!response.IsSuccess())
                return DisplayError(response);

            if (settings.OutputJson)
                DisplayJson(response.Data);
            else
            {
                switch (settings.OverrideMode)
                {
                    case OverrideMode.Indefinite:
                    {
                        AnsiConsole.MarkupLine($"[green]Override door mode was set to[/] [yellow]{settings.DoorMode}[/] [green]until[/] [yellow]cancelled[/]");
                        break;
                    }
                    case OverrideMode.Minutes:
                    {
                        var timeSpan = DateTime.Now.AddMinutes(settings.Minutes.Value);
                        AnsiConsole.MarkupLine($"[green]Override door mode was set to[/] [yellow]{settings.DoorMode}[/] [green]until[/] [yellow]{timeSpan}[/]");
                        break;
                    }
                    case OverrideMode.TimeOfDay:
                    {
                        var timeSpan = settings.TimeOfDay - DateTime.Today.AtMidnight();
                        AnsiConsole.MarkupLine($"[green]Override door mode was set to[/] [yellow]{settings.DoorMode}[/] [green]until[/] [yellow]{timeSpan}[/]");
                        break;
                    }
                    case OverrideMode.Seconds:
                    {
                        var timeSpan = DateTime.Now.AddMinutes(settings.Seconds.Value);
                        AnsiConsole.MarkupLine($"[green]Override door mode was set to[/] [yellow]{settings.DoorMode}[/] [green]until[/] [yellow]{timeSpan}[/]");
                        break;
                    }
                };
            }

            return CommandLineSuccess;
        }

        #endregion

        private int GetTimeInterval(OverrideDoorModeSettings settings)
        {
            if (!settings.OverrideMode.HasValue)
                return 0;

            var duration = 0;
            
            switch (settings.OverrideMode)
            {
                case OverrideMode.Indefinite:
                    duration = 0;
                    break;
                case OverrideMode.Minutes:
                    duration = settings.Minutes ?? 60;
                    break;
                case OverrideMode.TimeOfDay:
                    var span = (settings.TimeOfDay ?? DateTime.Now.AddHours(1)) - DateTime.Today.AtMidnight();
                    duration = (int)span.TotalMinutes;
                    break;
                case OverrideMode.Seconds:
                    duration = 1;
                    break;
            };

            return (int)settings.OverrideMode << 14 | duration;
        }
    }
}
