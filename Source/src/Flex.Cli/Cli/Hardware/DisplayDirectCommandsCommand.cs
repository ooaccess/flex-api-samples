using System.Net;
using Flex.Cli.Hardware.Settings;
using Flex.DataObjects;
using Flex.DataObjects.Hardware;
using Flex.Services.Abstractions;
using Spectre.Console;
using Spectre.Console.Cli;

namespace Flex.Cli.Hardware
{
    internal class DisplayDirectCommandsCommand : AsyncCommand<HardwareSettings>
    {
        public DisplayDirectCommandsCommand(IOptionsProvider options, ICacheStore cache, IFlexHttpClientFactory factory)
            : base(options, cache, factory)
        {
        }

        #region Overrides of AsyncCommand<DoorModeSettings>

        protected override async Task<int> ExecuteAsync(CommandContext context, HardwareSettings settings, HttpClient client, UserInfo userInfo)
        {
            var response = await AnsiConsole
                .Status()
                .StartAsync("Retrieving direct commands ...", _ => client.GetJsendAsync("api/v2/hardware/directcommands"));

            if (!response.IsSuccess())
                return DisplayError(response);

            if (settings.OutputJson)
                DisplayJson(response.Data);
            else
            {
                var directCommands = response.Deserialize<DirectCommand[]>();
                OutputTable(directCommands);
            }

            return CommandLineSuccess;
        }

        #endregion

        /// <summary>
        /// Helper method to display the direct commands in a table format in the console window
        /// </summary>
        /// <param name="directCommands"></param>
        protected static void OutputTable(params DirectCommand[] directCommands)
        {
            var table = new Table()
                .Title("Direct Commands")
                .BorderColor(Color.Red)
                .AddColumn("Unique Key")
                .AddColumn("Category")
                .AddColumn("Name");

            foreach (var item in directCommands)
            {
                table.AddRow(
                    item.UniqueKey.ToString(),
                    item.Category ?? "Default",
                    item.Name);
            }

            table.Expand();

            AnsiConsole.Write(table);
        }
    }
}
